import django
from django.conf.urls import url
from django.contrib import admin
from django.urls import include, path
from django.views.decorators.csrf import csrf_exempt
from graphene_django.views import GraphQLView
from rest_framework_jwt.views import obtain_jwt_token
from rest_framework_jwt.views import verify_jwt_token
#
from app import settings


urlpatterns = [
    path('admin/', admin.site.urls),
    path('graphql/', csrf_exempt(GraphQLView.as_view(graphiql=True))),
    url(r'^static/(?P<path>.*)$', django.views.static.serve,
        {'document_root': settings.URL_STATIC_ROOT}),
    url(r'^media/(?P<path>.*)$', django.views.static.serve,
        {'document_root': settings.URL_MEDIA_ROOT}),
]
