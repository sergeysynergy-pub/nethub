from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
#
# from app.settings import AUTH_USER_MODEL
from .models import User
# End of import section

# Register users admin pages using UserAdmin contrib class
admin.site.register(User, BaseUserAdmin)
