import os
from django.db import models
from django.contrib.auth.models import AbstractUser
# End of import section

class User(AbstractUser):
    def save(self, *args, **kwargs):
        """Overriding save model method"""

        if not self.pk:
            # This code only happens if the object is not in the database yet.
            # Otherwise it would have pk
            print("Creating new user")

            # Create user directory at the git server if it doesn't exists
            user_path = '/git/%s' % (self.username)
            if not os.path.exists(user_path):
                os.makedirs(user_path)

            # Call the "real" save() method
            super(User, self).save(*args, **kwargs)

        else:
            # Call the "real" save() method
            super(User, self).save(*args, **kwargs)
