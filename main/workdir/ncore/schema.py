import graphene
import graphql_relay
import subprocess
import os
from graphene import relay, ObjectType
from graphene_django import DjangoObjectType
from .helpers import update_create_instance
import django_filters
from graphene_django.filter import DjangoFilterConnectionField
from .models import Project, Slide
from .helpers import get_object, get_errors, update_create_instance
from users.schema import UserNode
#
from app.settings import GIT_ADDRESS


""" GraphQL filters """

class ProjectFilter(django_filters.FilterSet):
    class Meta:
        model = Project
        fields = ['title', 'user__username']


""" GraphQL/Relay Nodes """

class SlideNode(DjangoObjectType):
    class Meta:
        model = Slide


class SlideNodeInput(graphene.InputObjectType):
    header = graphene.String()


class ProjectNode(DjangoObjectType):
    class Meta:
        model = Project
        filter_fields = {
            'title': ['exact'],
            'user__username': ['exact'],
        }
        interfaces = (relay.Node, )


""" GraphQL Mutations """

class CreateSlide(graphene.Mutation):
    slide = graphene.Field(SlideNode)

    class Arguments:
        input = SlideNodeInput(required=True)

    @staticmethod
    def mutate(root, info, input=None):
        # Making authentication
        user = info.context.user or None
        print('USER', user)
        if user.is_anonymous:
            raise Exception('Not logged in!')

        slide = Slide(header=input.header)
        slide.save()
        return CreateSlide(slide=slide)


class CreateProject(relay.ClientIDMutation):
    project = graphene.Field(ProjectNode)

    class Input:
        title = graphene.String()
        library = graphene.String()

    def mutate_and_get_payload(root, info, **input):
        title = input.get('title')
        library = input.get('library')
        user = info.context.user

        # Check user authentication
        if user.is_anonymous:
            raise Exception('Not logged in!')

        project = Project(
            title = title,
            user = user,
            library = library,
            )
        project.save()

        return CreateProject(project=project)


class CreateFork(relay.ClientIDMutation):
    fork_project = graphene.Field(ProjectNode)

    class Input:
        project_id = graphene.ID()

    def mutate_and_get_payload(root, info, **input):
        user = info.context.user
        id = input.get('project_id')

        if user.is_anonymous:
            raise Exception('Not loggen in!')

        _, pk = graphql_relay.from_global_id(id)

        project = Project.objects.get(pk=pk)

        if not project:
            raise Exception('Invalid Project')

        user_projects = Project.objects.filter(user=user)
        orig_net_title, orig_net_owner = project.title, project.user.username

        # TODO: refactor

        check = True
        new_title = orig_net_title

        while check:
            if len(user_projects) > 0:
                for i in range(len(user_projects)):
                    if user_projects[i].title == new_title:

                        last_two_chars = user_projects[i].title[-2:]

                        if last_two_chars[0] == "-":

                            try:
                                copy_num = int(last_two_chars[1]) + 1
                                new_title = user_projects[i].title[:-1] + str(copy_num)
                                break
                            except ValueError:
                                new_title += "-1"

                        else:
                            new_title += "-1"
                            check = True
                            break
                    else:
                        check = False
            else:
                check = False


        project.title = new_title
        project.pk = None
        project.user = user
        project.git_address = '%s/%s/%s.git' % (GIT_ADDRESS, project.user.username, new_title)
        project.save()

        command = 'cd /git && cp -R %s/%s.git /tmp' % \
            (orig_net_owner, orig_net_title)

        subprocess.call(command, shell=True)

        if orig_net_title != project.title:
            command = 'cd /tmp && mv %s.git %s.git' % \
                (orig_net_title, project.title)

            subprocess.call(command, shell=True)

        command = 'mv "/tmp/%s.git" /git/%s/%s.git' % \
            (project.title, project.user.username, project.title)

        subprocess.call(command, shell=True)


class CreateMerge(relay.ClientIDMutation):
    master_project = graphene.Field(ProjectNode)
    second_project = graphene.Field(ProjectNode)

    class Input:
        master_id = graphene.ID()
        second_id = graphene.ID()

    def mutate_and_get_payload(root, info, **input):
        user = info.context.user
        master_id = input.get('master_id')
        second_id = input.get('second_id')

        if user.is_anonymous:
            raise Exception('Not loggen in!')

        _, pk_master = graphql_relay.from_global_id(master_id)
        _, pk_second = graphql_relay.from_global_id(second_id)

        master_project = Project.objects.get(pk=pk_master)
        second_project = Project.objects.get(pk=pk_second)

        if not master_project:
            raise Exception('Invalid Master Project')

        if not second_project:
            raise Exception('Invalid Second Project')

        master_net_title, master_net_owner = master_project.title, master_project.user.username
        second_net_title, second_net_owner = second_project.title, second_project.user.username

        # TODO: git server adress
        master_git_server = 'http://main_git:5000/%s/%s.git' % \
            (master_net_owner, master_net_title)

        second_git_server = 'http://main_git:5000/%s/%s.git' % \
            (second_net_owner, second_net_title)

        command = "cd /tmp" + \
                  " && git clone "+master_git_server+\
                  " && git clone "+second_git_server+\
                  " && nhm merge "+master_net_title + " " + second_net_title + \
                  " && cd "+master_net_title + \
                  ' && git add . && git commit -m "merge '+second_net_title+' to '+master_net_title+'"' + \
                  " && git push && cd .. && rm -R "+master_net_title+" "+second_net_title

        subprocess.call(command, shell=True)


class Query(ObjectType):
    node = relay.Node.Field()  # root node field

    project = DjangoFilterConnectionField(
        ProjectNode,
        filterset_class=ProjectFilter,
    )

    allProjects = graphene.List(ProjectNode)
    allSlides = graphene.List(SlideNode)

    def resolve_allSlides(self, info, **kwargs):
        return Slide.objects.filter(is_deleted=False).order_by('order')

    def resolve_allProjects(self, info, **kwargs):
        return Project.objects.order_by('order')


class DeleteProject(relay.ClientIDMutation):
    class Input:
        id = graphene.String(required=True)

    errors = graphene.List(graphene.String)
    owner = graphene.Field(UserNode)

    def mutate_and_get_payload(root, info, **input):
        user = info.context.user
        # Check for user logged in
        if user.is_anonymous:
            raise Exception('Not loggen in!')

        try:
            project = get_object(Project, input['id'])

            # Check project owner
            if info.context.user != project.user:
                raise Exception('Not project owner!')

            if project:
                project.delete()
                return DeleteProject(owner=info.context.user)
        except:
            return DeleteProject(owner=None)
        # except ValidationError as e:
            # return DeleteProject(deleted_project=None, errors=get_errors(e))



class Mutation(graphene.ObjectType):
    create_slide = CreateSlide.Field()
    fork_project = CreateFork.Field()
    merge_project = CreateMerge.Field()
    create_project = CreateProject.Field()
    delete_project = DeleteProject.Field()
