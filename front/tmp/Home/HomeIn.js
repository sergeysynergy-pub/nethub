import React, { Component } from 'react'
import styled from 'styled-components'
//
import { media } from '../../constants'
import NetsList from '../Nets/NetsList'


const StyledHomeIn = styled.div`
  margin: 2em 2em 4em 2em;

  ${media.tablet`
  `}
`

export default class Home extends Component {
  render() {
    return (
      <StyledHomeIn>
        <NetsList />
      </StyledHomeIn>
    );
  }
}
