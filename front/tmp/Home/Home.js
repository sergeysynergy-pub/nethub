import React from 'react'
//
import HomeIn from './HomeIn'
import HomeOut from './HomeOut'


const Home = ({ userID, username, _logOut }) => <div>
  {Number(userID) !== 0 ?
    <HomeIn
      userID={userID}
      username={username}
      _logOut={_logOut}
    />
  :
    <HomeOut />
  }
</div>


export default Home
