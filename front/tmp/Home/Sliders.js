import React, { Component } from 'react'
import { QueryRenderer, graphql } from 'react-relay'
import { UncontrolledCarousel } from 'reactstrap'
//
import environment from '../../Environment'
import { BACK } from '../../constants'


const query = graphql`
  query SlidersQuery{
    allSlides {
      id
      header
      image
    }
  }
`


export default class Sliders extends Component {
  render() {
    return <QueryRenderer
      environment={environment}
      query={query}
      variables={{}}
      render={({error, props}) => {
        if (error) {
          return <div>{error.message}</div>
        } else if (props) {
          let slides = []
          if (props.allSlides != null) {
            slides = props.allSlides.map(item => {
              return {
                src: `${BACK}/media/${item.image}`,
                caption: '',
                header: item.header,
              }
            })
          }

          return <div>
            <UncontrolledCarousel items={slides} />
          </div>
        }
        return <div>HetHub - smart AI solution for your business</div>
      }}
    />
  }
}
