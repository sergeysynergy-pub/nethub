import React, { Component } from 'react'
import { QueryRenderer, graphql } from 'react-relay'
import styled from 'styled-components'
import { Icon } from 'react-icons-kit'
import {ic_input} from 'react-icons-kit/md/ic_input'
import { Button } from 'reactstrap'
import { Table } from 'reactstrap'
import { Link } from 'react-router-dom'
//
import { media } from '../../constants'
import environment from '../../Environment'
import CreateMergeMutation from '../../mutations/CreateMergeMutation'


const StyledMergeNets = styled.div`
  font-size: .8em;

  ${media.tablet`
  `}
`


const query = graphql`
  query MergeNetsQuery($username: String!) {
    user(username: $username) {
      edges {
        node {
          id
          username
          projectSet {
            edges {
              node {
                id
                title
                library
              }
            }
          }
        }
      }
    }
  }
`


export default class User extends Component {
  merge = (masterId, secondId) => {
    CreateMergeMutation(masterId, secondId, (result) => {
      if (result) {
        this.props.history.push(`/users/${this.props.username}`)
      } else {
        this.setState({warning: 'Try again'})
      }
    })
  }

  render() {
    const username = this.props.username

    return <QueryRenderer
      environment={environment}
      query={query}
      variables={{username: username}}
      render={({error, props}) => {
        if (error) {
          return <div>{error.message}</div>
        } else if (props) {
          // console.log('TOP props', props);
          const edges = props.user.edges[0].node.projectSet.edges
          const nets = []
          let counter = 0
          edges.forEach(({node}, key) => {
            if (node.title !== this.props.title) {
              nets.push(<NetsRow
                key={key}
                counter={counter++}
                title={node.title}
                library={node.library}
                username={username}
                masterId={this.props.masterId}
                secondId={node.id}
                _merge={this.merge}
                />
              )
            }
          })

          return <StyledMergeNets>
            <br />
            <br />
            <h2>Merge into this neuronet:</h2>
            <Table>
              <thead>
                <tr>
                  <th>#</th>
                  <th>Title</th>
                  <th>Library</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                {nets}
              </tbody>
            </Table>
          </StyledMergeNets>
        }
        return <div></div>
      }}
    />
  }
}


const NetsRow = ({
        counter, title, username, library, masterId, secondId,  _merge}) =>
  <tr>
    <th scope="row">{++counter}</th>
    <td>
      <Link to={'/users/' + username + '/' + title}>{title}</Link>
    </td>
    <td>{library}</td>
    <td>
      <Button color="primary" onClick={() => _merge(masterId, secondId)} >
        <Icon icon={ic_input} />{ }Merge
      </Button>
    </td>
  </tr>
