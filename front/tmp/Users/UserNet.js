import React, { Component } from 'react'
import { QueryRenderer, graphql } from 'react-relay'
import styled from 'styled-components'
import { Icon } from 'react-icons-kit'
import {codeFork} from 'react-icons-kit/fa/codeFork'
import { Button } from 'reactstrap'
//
import { media } from '../../constants'
import environment from '../../Environment'
import Header from './Header'
import MergeNets from './MergeNets'
import CreateForkMutation from '../../mutations/CreateForkMutation'


const StyledUserNet = styled.div`
  .content {
    padding: 1em 3em;
    font-size: 1.4em;
    h1 {
      font-size: 1.4em;
      border-bottom: solid 1px #ccc;
      padding: 0 0 .4em 0;
    }
    h2 {
      font-size: 1.1em;
      padding: 0 0 .4em 0;
      margin: 1.4em 0;
    }
    button {
      margin: 0 0 0 1em;
      padding: .2em .6em .2em .4em;
      font-size: .6em;
      font-weight: bold;
    }
  }

  ${media.tablet`
  `}
`


const query = graphql`
  query UserNetQuery($username: String!, $title: String!) {
    project(user_Username: $username, title: $title) {
      edges {
        node {
          id
          title
          library
          user {
            id
          }
        }
      }
    }
  }
`


export default class User extends Component {
  username = this.props.match.params.username
  title = this.props.match.params.title

  fork = (netId) => {
    CreateForkMutation(netId, (result) => {
      if (result) {
        this.props.history.push(`/users/${this.props.username}`)
      } else {
        this.setState({warning: 'Try again'})
      }
    })
  }

  render() {

    return <QueryRenderer
      environment={environment}
      query={query}
      variables={{username: this.username, title: this.title}}
      render={({error, props}) => {
        if (error) {
          return <div>{error.message}</div>
        } else if (props) {
          // console.log('TOP props', props);
          const node = props.project.edges[0].node

          return <StyledUserNet>
            <Header username={this.username} nets={true} />
            <div className='content'>
              <h1>
                {this.username}/{this.title}
                {this.props.userID !== node.user.id &&
                  <Button color="primary" onClick={() => this.fork(node.id)} >
                    <Icon icon={codeFork} />
                    Fork
                  </Button>
                }
              </h1>
              <div>
                Library: <strong>{node.library}</strong>
              </div>
              {this.props.userID === node.user.id &&
                <MergeNets
                  history={this.props.history}
                  username={this.username}
                  title={this.title}
                  masterId={node.id}
                />
              }
            </div>
          </StyledUserNet>
        }
        return <div></div>
      }}
    />
  }
}
