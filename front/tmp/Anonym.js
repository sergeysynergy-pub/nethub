import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Route, Switch } from 'react-router-dom'
import styled from 'styled-components'
//
import { media } from '../../constants'
import Header from './Header'
import Home from './Home'
import Signin from '../includes/Signin'
import Register from '../includes/Register'
import Bottom from '../includes/Bottom'
/* End of import section */

const StyledAnonym = styled.div`
  ${media.tablet`
  `}
`


export default class Anonym extends Component {
  render() {
    return (
      <StyledAnonym>
        <div className='layout'>
          <div className='header'>
            <Header
              userID={this.props.userID}
              username={this.props.username}
            />
          </div>
          <div className='content'>
            <Switch>
              <Route exact path="/" component={(props) => (
                <Home
                  userID={this.props.userID}
                {...props} />)}
              />
              <Route exact path="/sign_in" component={(props) => (
                <Signin
                  _saveUserData={this.props._saveUserData}
                {...props} />)}
              />
              <Route exact path="/register" component={(props) => (
                <Register
                  _saveUserData={this.props._saveUserData}
                {...props} />)}
              />
            </Switch>
          </div>
          <div className='bottom'>
            <Bottom />
          </div>
        </div>
      </StyledAnonym>
    )
  }
}
Anonym.propTypes = {
  username: PropTypes.string,
  userID: PropTypes.string,
  _saveUserData: PropTypes.func,
}
