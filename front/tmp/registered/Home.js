import React, { Component } from 'react'
import styled from 'styled-components'
//
import { media } from '../../constants'
import Marketing from '../includes/Marketing'


const StyledHome = styled.div`
  .carousel .carousel-item {
    border: dashed 2px greeen;
  }

  ${media.tablet`
  `}
`

export default class Home extends Component {
  render() {
    return (
      <StyledHome>
        <Marketing />
      </StyledHome>
    );
  }
}
