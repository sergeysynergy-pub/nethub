import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Route, Switch } from 'react-router-dom'
import styled from 'styled-components'
//
import { media } from '../../constants'
import Header from './Header'
import Home from './Home'
// import User from './Users/User'
// import UserNet from './Users/UserNet'
// import Nets from './Nets/Nets'
// import NetAdd from './Nets/NetAdd'

// import Signin from '../includes/Signin'
// import Register from '../includes/Register'
import Explore from '../includes/Explore'
import Bottom from '../includes/Bottom'
/* End of import section */

const StyledRegistered = styled.div`
  ${media.tablet`
  `}
`


export default class Registered extends Component {
  render() {
    console.log('userID', this.props.userID);
    return (
      <StyledRegistered>
        <div className='layout'>
          <div className='header'>
            <Header {...this.props} />
          </div>
          <div className='content'>
            <Switch>
              <Route exact path="/" component={(props) => (
                <Home
                {...props} />)}
              />
              <Route exact path="/explore" component={(props) => (
                <Explore
                {...props} />)}
              />
              {/*
              <Route path="/users/:username/:title" component={(props) => (
                <UserNet
                  userID={this.state.userID}
                  username={this.state.username}
                {...props} />)}
              />
              <Route path="/users/:username" component={(props) => (
                <User
                {...props} />)}
              />
              <Route exact path="/nets" component={(props) => (
                <Nets
                {...props} />)}
              />
              <Route exact path="/netadd" component={(props) => (
                <NetAdd
                {...props} />)}
              />
              */}
            </Switch>
          </div>
          <div className='bottom'>
            <Bottom />
          </div>
        </div>
      </StyledRegistered>
    )
  }
}
Registered.propTypes = {
  username: PropTypes.string,
  userID: PropTypes.string,
  _logOut: PropTypes.func,
}
