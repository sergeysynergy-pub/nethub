import React, { Component } from 'react'
import { Route, Switch } from 'react-router-dom'
import styled from 'styled-components'
//
import { media } from '../constants'
import Anonym from './anonym/Anonym'
// import Registered from './registered/App'
import Header from './Header/Header'
import Home from './Home/Home'
import Signin from './Login/Signin'
import Register from './Login/Register'
import User from './Users/User'
import UserNet from './Users/UserNet'
// import Nets from './Nets/Nets'
// import NetAdd from './Nets/NetAdd'
import Bottom from './Bottom/Bottom'


const StyledApp = styled.div`
  ${media.tablet`
  `}
`


export default class App extends Component {
  // Try to load user data from local storage cache
  constructor(props) {
    super()

    this.userID = localStorage.getItem('USER_ID')
    this.userToken = localStorage.getItem('USER_TOKEN')
    this.username = localStorage.getItem('USER_NAME')

    // console.log('this token type', typeof this.userToken);
    /*
    if (typeof this.userToken === 'undefined') {
      this.userToken = 'bla'
    }
    console.log('token type', typeof this.userToken);
    */

    this.state = {
      userID: this.userID == null? 0 : this.userID,
      // userToken: this.userToken == null? '' : this.userToken,
      userToken: this.userToken,
      username: this.username,
    }
  }

  saveUserData = (id, token, username) => {
    // Updating local storage user data
    localStorage.setItem('USER_ID', id)
    localStorage.setItem('USER_TOKEN', token)
    localStorage.setItem('USER_NAME', username)
    this.setState({
      userID: id,
      username: username,
      userToken: token,
    })
  }

  logOut = () => {
    // Updating local storage user data
    localStorage.setItem('USER_ID', 0)
    localStorage.setItem('USER_TOKEN', '')
    localStorage.setItem('USER_NAME', '')
    this.setState({userID: 0, userToken: ''})
  }

  render() {
    console.log('ID', this.state.userID);
    console.log('TOKEN', this.state.userToken);
    return (
      <StyledApp>
        {Number(this.state.userID) === 0 ?
          <Anonym />
          :
          <Anonym />
          /*
          <Registered
            userID={this.state.userID}
            username={this.state.username}
            _logOut={this.logOut}
          />
          */
        }
        <div className='layout'>

          <div className='header'>
            <Header
              userID={this.state.userID}
              username={this.state.username}
              _logOut={this.logOut}
            />
          </div>
          <div className='content'>
            <Switch>
              <Route exact path="/" component={(props) => (
                <Home
                  userID={this.state.userID}
                {...props} />)}
              />
              <Route exact path="/sign_in" component={(props) => (
                <Signin
                  _saveUserData={this.saveUserData}
                {...props} />)}
              />
              <Route exact path="/register" component={(props) => (
                <Register
                  _saveUserData={this.saveUserData}
                {...props} />)}
              />
              <Route path="/users/:username/:title" component={(props) => (
                <UserNet
                  userID={this.state.userID}
                  username={this.state.username}
                {...props} />)}
              />
              <Route path="/users/:username" component={(props) => (
                <User
                {...props} />)}
              />
              {/*
              <Route exact path="/nets" component={(props) => (
                <Nets
                {...props} />)}
              />
              <Route exact path="/netadd" component={(props) => (
                <NetAdd
                {...props} />)}
              />
              */}
            </Switch>
          </div>
          <div className='bottom'>
            <Bottom />
          </div>
        </div>
      </StyledApp>
    )
  }
}
