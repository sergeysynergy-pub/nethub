import React, { Component } from 'react'
import styled from 'styled-components'
//
import { media } from '../../constants'

const StyledBottom = styled.div`
  margin: 4em 0 .4em 0;
  text-align: center;
  color: #bbb;

  ${media.tablet`
    width: 100%;
  `}
`

export default class Header extends Component {
  render() {
    return (
      <StyledBottom>
        Let nethack begin!
      </StyledBottom>
    )
  }
}
