import React, { Component } from 'react'
import styled from 'styled-components'
import {
  Jumbotron, Input, FormGroup, Button, Label, Alert,
} from 'reactstrap'
//
import { media } from '../../constants'
// import CreateNetMutation from '../../mutations/CreateNetMutation'


const StyledNetAdd = styled.div`
  // border: dashed 2px green;
  margin: 2em 0 0 0;
  .jumbotron {
    max-width: 350px;
    margin: 0 auto;
  }
  button {
    margin: 0 0 1em 0;
  }
  a.Link:hover,
  a.Link {
    padding: 0.2em 1em;
    text-decoration: underline;
    color: inherit;
    cursor: pointer;
  }

  ${media.tablet`
    margin: 0 .6em;
  `}
`


export default class SignIn extends Component {
  state = {
    title: '',
    warning: '',
  }

  render() {
    return (
      <StyledNetAdd>
        <Jumbotron>
          <h2>Create network</h2>
          <FormGroup>
            <Label>Network name</Label>
            <Input
              type='text'
              value={this.state.username}
              placeholder='title'
              onChange={(e) => this.setState({ title: e.target.value })}
            />
          </FormGroup>
          <Button
            color="primary"
            onClick={() => this.confirm()}
          >
            Create
          </Button>
          {this.state.warning.length > 0 &&
            <Alert color="warning">
              {this.state.warning}
            </Alert>
          }
        </Jumbotron>
      </StyledNetAdd>
    )
  }

  confirm = () => {
    const { title, userID } = this.state
    if (title.length < 2) {
      this.setState({warning: 'Two letters at least'})
    } else {
      /*
      CreateNetMutation(title, userID, (result) => {
        if (result) {
          this.props.history.push(`/nets`)
        } else {
          this.setState({warning: 'Try again'})
        }
      })
      */
      this.setState({warning: 'Comming soon...'})
    }
  }
}
