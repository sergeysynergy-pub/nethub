import React, { Component } from 'react'
import { QueryRenderer, graphql } from 'react-relay'
import styled from 'styled-components'
import { Table } from 'reactstrap'
import { Link } from 'react-router-dom'
//
import environment from '../../Environment'
import { media } from '../../constants'


const StyledNetsList = styled.div`

  ${media.tablet`
  `}
`

const query = graphql`
  query NetsListQuery {
    allProjects{
      id
      title
      library
      user {
        id
        username
      }
    }
  }
`


export default class NetsList extends Component {
  render() {
    return <QueryRenderer
      environment={environment}
      query={query}
      variables={{}}
      render={({error, props}) => {
        if (error) {
          return <div>{error.message}</div>
        } else if (props) {
          let projects = []
          if (props.allProjects != null) {
            projects = props.allProjects.map((item, key) =>(
              <NetsRow
                key={key}
                counter={key}
                title={item.title}
                library={item.library}
                username={item.user.username}
                />
            ))
          }

          return <StyledNetsList>
            <h1>Neuronets</h1>
            <Table>
              <thead>
                <tr>
                  <th>#</th>
                  <th>Title</th>
                  <th>Library</th>
                  <th>User</th>
                </tr>
              </thead>
              <tbody>
                {projects}
              </tbody>
            </Table>
          </StyledNetsList>
        }
        return <div></div>
      }}
    />
  }
}


const NetsRow = ({counter, title, username, library}) =>
  <tr>
    <th scope="row">{++counter}</th>
    <td>
      <Link to={'/users/' + username + '/' + title}>{title}</Link>
    </td>
    <td>{library}</td>
    <td>
      <Link to={'/users/' + username }>{username}</Link>
    </td>
  </tr>
