import React, { Component } from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'
import {
  Jumbotron, Input, FormGroup, Button, Label, Alert,
} from 'reactstrap'
//
import { media } from '../../constants'
import SigninUserMutation from '../../mutations/SigninUserMutation'


const StyledSignIn = styled.div`
  // border: dashed 2px green;
  margin: 2em 0 0 0;
  .jumbotron {
    max-width: 350px;
    margin: 0 auto;
  }
  button {
    margin: 0 0 1em 0;
  }
  a.Link:hover,
  a.Link {
    padding: 0.2em 1em;
    text-decoration: underline;
    color: inherit;
    cursor: pointer;
  }

  ${media.tablet`
    margin: 0 .6em;
  `}
`


export default class SignIn extends Component {
  state = {
    password: '',
    username: '',
    warning: '',
  }

  render() {
    return (
      <StyledSignIn>
        <Jumbotron>
          <h2>Sign in / <Link to='/register'>Register</Link></h2>
          <FormGroup>
            <Label>Username</Label>
            <Input
              type="text"
              value={this.state.username}
              placeholder="username"
              onChange={(e) => this.setState({ username: e.target.value })}
            />
          </FormGroup>
          <FormGroup>
            <Label>Password</Label>
            <Input
              type="password"
              value={this.state.password}
              placeholder='password'
              onChange={(e) => this.setState({ password: e.target.value })}
            />
          </FormGroup>
          <Button
            color="primary"
            onClick={() => this.confirm()}
          >
            Sign in
          </Button>
          {this.state.warning.length > 0 &&
            <Alert color="warning">
              {this.state.warning}
            </Alert>
          }
        </Jumbotron>
      </StyledSignIn>
    )
  }

  confirm = () => {
    const { username, password } = this.state
    if (username.length < 2 || password.length < 2) {
      this.setState({warning: 'Please input correct sign in data'})
    } else {
      SigninUserMutation(username, password,
            (result, id, token, username) => {
        if (result) {
          this.props._saveUserData(id, token, username)
          this.props.history.push(`/`)
        } else {
          this.setState({warning: 'Try again'})
        }
      })
    }
  }
}
