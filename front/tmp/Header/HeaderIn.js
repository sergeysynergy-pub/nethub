import React, { Component } from 'react'
import styled from 'styled-components'
import { NavLink } from 'reactstrap'
import { NavLink as RRNavLink } from 'react-router-dom'
import { Icon } from 'react-icons-kit'
import { androidShareAlt } from 'react-icons-kit/ionicons/androidShareAlt'
import {
  Collapse,
  Navbar,
  NavbarToggler,
  Nav,
  NavItem,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from 'reactstrap'
//
import { media } from '../../constants'


const StyledHeaderIn = styled.div`
  // border: dashed 2px green;
  border-bottom: solid 2px #ccc;
  .navbar {
    padding: .1em .8em .1em .4em;
    background-color: #cc66ff;
    a, a:visited {
      color: white;
      font-weight: bold;
    }
  }
  .logo {
    margin: 0 .6em 0 0;
  }
  .logout {
    cursor:pointer;
  }

  ${media.tablet`
    width: 100%;
  `}
`


export default class Header extends Component {
  state = {
      isOpen: false,
  }
  toggle = () => {
    this.setState({
      isOpen: !this.state.isOpen
    })
  }

  render() {
    return (
      <StyledHeaderIn>
        <Navbar dark expand="md">
          <NavLink
            to="/"
            activeClassName="active"
            tag={RRNavLink}
          >
            <Icon icon={androidShareAlt} className='logo' />
            NetHub
          </NavLink>
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav className="ml-auto" navbar>
              <NavItem>
                <NavLink disabled href="#">|</NavLink>
              </NavItem>
              <UncontrolledDropdown nav inNavbar>
                <DropdownToggle nav caret>
                  {this.props.username}
                </DropdownToggle>
                <DropdownMenu right>
                  <DropdownItem
                    className='logout'
                    onClick={this.props._logOut}
                    >
                    LogOut
                  </DropdownItem>
                </DropdownMenu>
              </UncontrolledDropdown>
            </Nav>
          </Collapse>
        </Navbar>
      </StyledHeaderIn>
    )
  }
}
