import React from 'react'
//
import HeaderIn from './HeaderIn'
import HeaderOut from './HeaderOut'


const Header = ({ userID, username, _logOut }) => <>
  {Number(userID) !== 0 ?
    <HeaderIn
      userID={userID}
      username={username}
      _logOut={_logOut}
    />
  :
    <HeaderOut />
  }
</>


export default Header
