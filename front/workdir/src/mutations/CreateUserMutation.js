import {
  commitMutation,
  graphql
} from 'react-relay'
//
import environment from '../Environment'


const mutation = graphql`
  mutation CreateUserMutation($username: String!,
              $email: String!, $password: String!) {
    createUser(username: $username, password: $password, email: $email) {
      user {
        id
        username
      }
    }

    tokenAuth(username: $username, password: $password) {
      token
    }
  }
`


export default (username, email, password, callback) => {
  const variables = {
    username,
    password,
    email,
    clientMutationId: '',
  }

  commitMutation(
    environment,
    {
      mutation,
      variables,
      onCompleted: (response) => {
        if (response.createUser) {
          const data = {
            token: response.tokenAuth.token,
            username: response.createUser.user.username,
            id: response.createUser.user.id,
          }
          callback(true, data, null)
        } else {
          callback(false, null, response)
        }
      },
      onError: err => console.error(err),
    },
  )
}
