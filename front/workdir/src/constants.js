import { css } from 'styled-components'


// Iterate through the sizes and create a media template
const sizes = {
  tablet: 768,
}
export const media = Object.keys(sizes).reduce((acc, label) => {
  acc[label] = (...args) => css`
    @media (max-width: ${sizes[label] / 16}em) {
      ${css(...args)}
    }
  `
  return acc
}, {})

export const ROLE = {USER: 'User', ANONYM: 'Anonym'}

export const GRAPHQL_API = 'http://127.0.0.1:21020/graphql/'
export const BACK = 'http://127.0.0.1:21020'
