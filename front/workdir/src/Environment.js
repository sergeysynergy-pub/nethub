import {
  Environment, Network, RecordSource, Store
} from "relay-runtime"
//
import { GRAPHQL_API } from './constants'


const store = new Store(new RecordSource())

const network = Network.create((operation, variables) => {
  let userToken = localStorage.getItem('USER_TOKEN')

  // Token validation
  if (typeof userToken !== 'string') {
    userToken = ''
  } else if (userToken === 'undefined') {
    userToken = ''
  }

  return fetch(GRAPHQL_API, {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': `JWT ${userToken}`,
    },
    body: JSON.stringify({
      query: operation.text,
      variables,
    }),
  }).then(response => {
    return response.json()
  })
})

const environment = new Environment({
  network,
  store,
})

export default environment
