import React, { Component } from 'react'
import { QueryRenderer, graphql } from 'react-relay'
import { Link } from 'react-router-dom'
import styled from 'styled-components'
import { Icon } from 'react-icons-kit'
import {codeFork} from 'react-icons-kit/fa/codeFork'
import { Button, Input, InputGroup } from 'reactstrap'
//
import { media } from '../../constants'
import environment from '../../Environment'
import MergeNets from './MergeNets'
import CreateForkMutation from '../../mutations/CreateForkMutation'
import DeleteProjectMutation from '../../mutations/DeleteProjectMutation'


const StyledUserNet = styled.div`
  .header h1 {
    font-size: 1.6em;
  }
  .fork {
    float: right;
    margin: 0 0 0 1em;
  }
  .input-group {
    margin: 1em 0;
    .legend {
      padding: .5em .6em 0 0;
    }
  }
  .delete {
    text-align: right;
  }

  ${media.tablet`
  `}
`


const query = graphql`
  query UserNetQuery($username: String!, $title: String!) {
    project(user_Username: $username, title: $title) {
      edges {
        node {
          id
          title
          library
          gitAddress
          user {
            id
          }
        }
      }
    }
  }
`


export default class User extends Component {
  username = this.props.match.params.username
  title = this.props.match.params.title

  fork = netId => {
    CreateForkMutation(netId, (result) => {
      if (result) {
        this.props.history.push(`/users/${this.props.username}`)
      } else {
        this.setState({warning: 'Try again'})
      }
    })
  }

  delete = netId => {
    DeleteProjectMutation(netId, result => {
      if (result) {
        console.log('RESULT', result);
        this.props.history.push(`/users/${this.props.username}`)
      } else {
        console.log('TRY AGAIN', result);
        // this.setState({warning: 'Try again'})
        this.props.history.push(`/users/${this.props.username}`)
      }
    })
  }

  render() {

    return <QueryRenderer
      environment={environment}
      query={query}
      variables={{username: this.username, title: this.title}}
      render={({error, props}) => {
        if (error) {
          return <div>{error.message}</div>
        } else if (props) {
          const node = props.project.edges[0].node

          return <StyledUserNet className="content">
            <div className="header">
              <h1>
                {this.props.userId !== node.user.id ?
                  <>
                    <Link to={'/users/' + this.username}>
                      {this.username}
                    </Link>
                    /<strong>{this.title}</strong>
                  <Button color="primary" onClick={() => this.fork(node.id)}
                    className="fork"
                  >
                    <Icon icon={codeFork} />
                    Fork
                  </Button>
                  </>
                :
                  <>
                  Your project: {this.title}
                  </>
                }
              </h1>
            </div>
            <div className="body">
              <InputGroup>
                <span className="legend">
                  Project url:
                </span>
                <Input
                  value={node.gitAddress}
                  disabled
                />
              </InputGroup>
              <div>
                Neuronet library used: <strong>{node.library}</strong>
              </div>
              {this.props.userId === node.user.id &&
              <div className="delete">
                <Button
                  outline
                  color="danger"
                  onClick={() => this.delete(node.id)}
                >Delete project</Button>
              </div>
              }
              {this.props.userId === node.user.id &&
                <MergeNets
                  history={this.props.history}
                  username={this.username}
                  title={this.title}
                  masterId={node.id}
                />
              }
            </div>
          </StyledUserNet>
        }
        return <div></div>
      }}
    />
  }
}
