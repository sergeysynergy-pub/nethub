import React from 'react'
import { Highlight } from 'react-fast-highlight'

class CodeBlock extends React.PureComponent {
  render() {
    return (
      <Highlight
        languages={[this.props.language]}
      >
        {this.props.value}
      </Highlight>
    )
  }
}

export default CodeBlock
