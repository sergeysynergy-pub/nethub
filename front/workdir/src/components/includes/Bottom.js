import React from 'react'
import styled from 'styled-components'
//
import { media } from '../../constants'
/* End of import section */

const StyledBottom = styled.div`
  margin: 4em 0 .4em 0;
  text-align: center;
  color: #bbb;

  ${media.tablet`
    width: 100%;
  `}
`

export default () => <StyledBottom>
  Let nethack begins!
</StyledBottom>
