import React, { Component } from 'react'
import { QueryRenderer, graphql } from 'react-relay'
import styled from 'styled-components'
import { Link } from 'react-router-dom'
//
import environment from '../../Environment'
import { media } from '../../constants'
import Tutorial from '../includes/Tutorial'
/* End of import section */


const StyledUser = styled.div`
  ${media.tablet`
  `}
`

const StyledNetsRow = styled.div`
  margin: 0 0 1em 0;
  padding: 1em 0;
  font-size: 1.4em;
  border-bottom: solid 1px #ccc;
  .meta {
    font-size: .7em;
    color: #777;
    a {
      color: #777;
    }
  }

  ${media.tablet`
  `}
`

const query = graphql`
  query UserQuery($username: String!) {
    user(username: $username) {
      edges {
        node {
          id
          username
          projectSet {
            edges {
              node {
                id
                title
                library
              }
            }
          }
        }
      }
    }
  }
`


export default class User extends Component {
  render() {
    const username = this.props.match.params.username

    return <QueryRenderer
      environment={environment}
      query={query}
      variables={{username: username}}
      render={({error, props}) => {
        if (error) {
          return <div>{error.message}</div>
        } else if (props) {
          // console.log('TOP props', props);

          const edges = props.user.edges[0].node.projectSet.edges
          const nets = edges.map(({node}, key) =>(
            <NetsRow
              key={key}
              title={node.title}
              library={node.library}
              username={username}
              />
          ))

          return <StyledUser className="content">
            <div className="header">
            {nets.length ?
              this.props.username === username ?
                <h1>Your neuronets projects</h1>
              :
                <h1>Projects</h1>
            :
              <h1>There are no any projects yet, try to create one</h1>
            }
            </div>
            <div className="body">
            {nets.length ?
              <>{nets}</>
            :
              <Tutorial />
            }
            </div>
          </StyledUser>
        }
        return <div></div>
      }}
    />
  }
}


const NetsRow = ({title, username, library}) => <StyledNetsRow>
  <Link to={'/users/' + username + '/' + title}>
    {username} /
    <strong>{title}</strong>
  </Link>
  <div className="meta">
    <span className="library">
      Library: {library}
    </span>
  </div>
</StyledNetsRow>
