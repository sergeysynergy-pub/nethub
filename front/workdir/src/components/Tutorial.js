import React, { Component } from 'react'
//
import Tutorial from './includes/Tutorial'
/* End of import section */


export default class extends Component {
  render() {
    return <div className="content">
      <div className="header">
        <h1>Tutorial</h1>
      </div>
      <div className="body">
        <Tutorial />
        <div>
          Quck testing - for everyone
        </div>
        <div>
          Quck learning - for the curious
        </div>
        <div>
          Documentation - for the developers
        </div>
        <div>
          Collaboration - for the investors
        </div>
      </div>
    </div>
  }
}
