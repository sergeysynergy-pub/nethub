import React, { Component } from 'react'
import styled from 'styled-components'
import { NavLink as RRNavLink } from 'react-router-dom'
import { Icon } from 'react-icons-kit'
import { androidShareAlt } from 'react-icons-kit/ionicons/androidShareAlt'
import {
  Collapse,
  Nav,
  Navbar,
  NavbarToggler,
  NavLink,
  NavItem,
} from 'reactstrap'
//
import { media } from '../../constants'

const StyledHeaderOut = styled.div`
  // border: dashed 2px green;
  border-bottom: solid 2px #ccc;
  .navbar {
    a, a:visited {
      color: #6600cc;
      font-weight: bold;
    }
    a:hover, a:active {
      color: #cc66ff;
    }
  }
  .logo {
    margin: 0 .6em 0 0;
  }
  .logout {
    cursor:pointer;
  }

  ${media.tablet`
    width: 100%;
  `}
`


export default class Header extends Component {
  state = {
      isOpen: false,
  }
  toggle = () => {
    this.setState({
      isOpen: !this.state.isOpen
    })
  }

  render() {
    console.log('header role', this.props.role);

    return (
      <StyledHeaderOut>
        <Navbar color="light" light expand="md">
          <NavLink
            to="/"
            activeClassName="active"
            tag={RRNavLink}
          >
            <Icon icon={androidShareAlt} className='logo' />
            NetHub
          </NavLink>
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav className="ml-auto" navbar>
              <NavLink
                to="/explore"
                activeClassName="active"
                tag={RRNavLink}
              >
                Explore
              </NavLink>
              <NavLink
                to="/tutorial"
                activeClassName="active"
                tag={RRNavLink}
              >
                Tutorial
              </NavLink>
              <NavItem>
                <NavLink disabled href="#">|</NavLink>
              </NavItem>
              <NavLink
                to="/sign_in"
                activeClassName="active"
                tag={RRNavLink}
              >
                Sign in
              </NavLink>
              <NavLink
                to="/register"
                activeClassName="active"
                tag={RRNavLink}
              >
                Register
              </NavLink>
            </Nav>
          </Collapse>
        </Navbar>
      </StyledHeaderOut>
    )
  }
}
