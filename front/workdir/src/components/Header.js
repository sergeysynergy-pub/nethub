import React, { Component } from 'react'
//
import { ROLE } from '../constants'
import AnonymHeader from './anonym/AnonymHeader'
import UserHeader from './user/UserHeader'
/* End of import section */

const {USER, ANONYM} = ROLE

export default class Header extends Component {
  render() {
    return <div>
      {this.props.role === ANONYM &&
        <AnonymHeader {...this.props} />
      }
      {this.props.role === USER &&
        <UserHeader {...this.props} />
      }
    </div>
  }
}
