#!/bin/bash

if [[ $1 = "test" ]]; then
  exit 1
fi

if [[ $1 = "init" ]]; then
  clear
  # Try to stop working containers
  docker-compose stop

  # Start all project containers:
  docker-compose -f docker-compose.yml -f docker-dev.yml up -d

  # Init db
  # docker exec nethub_main_maindb psql -U postgres -f maindb.sql

  # Initialize main django container
  docker exec nethub_main_django ./run.sh init

  # Initialize git container
  docker exec nethub_main_git yarn

  # Initialize front nodejs container
  docker exec nethub_front_nodejs ./run.sh init

  docker-compose stop
  ./launcher.sh start
  exit 1
fi

if [[ $1 = "start" ]]; then
  # clear
  # Start all project containers:
  docker-compose -f docker-compose.yml -f docker-dev.yml up -d

  # Start Django develop web-server
  # docker exec -d nethub_main_django ./run.sh

  # Start Uwsgi production web-server
  docker exec -d nethub_main_django ./runprod.sh
  
  # Start Flower - Celery workers monitor
  # docker exec -d nethub_main_django celery -A app flower

  # Start Git http server
  docker exec -d nethub_main_git ./run.sh

  # Start React develop web-server
  docker exec -d nethub_front_nodejs ./run.sh

  exit 1
fi

if [[ $1 = "rebuild" ]]; then
  clear
  echo "DO NOT run this command in parallel with other docker operations!!!"
  echo "clearing docker-cache..."
  echo "--------------------------------------------------------"
  docker rmi $(docker images -a --filter=dangling=true -q)
  echo "rebuilding..."
  docker-compose up -d --build
  # Start complex
  ./launcher.sh start
  exit 1
fi

# DEFAULT COMMANDS WITHOUT ARGUMENTS
./launcher.sh start
